-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: privyid_db
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gallery` (
  `gallery_id` int NOT NULL AUTO_INCREMENT,
  `users_id` int DEFAULT NULL,
  `gallery_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`gallery_id`),
  KEY `FK_UsersId` (`users_id`),
  CONSTRAINT `FK_UsersId` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (2,1,'tiktok.png'),(8,NULL,'tiktok.png'),(9,NULL,'tiktok.png'),(10,NULL,'tiktok.png'),(11,NULL,'tiktok.png'),(12,NULL,'tiktok.png');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riwayatkerja`
--

DROP TABLE IF EXISTS `riwayatkerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `riwayatkerja` (
  `riwayatkerja_id` int NOT NULL AUTO_INCREMENT,
  `users_id` int DEFAULT NULL,
  `jabatan` varchar(50) NOT NULL,
  `perusahaan` varchar(100) NOT NULL,
  `tahunsebelum` varchar(50) NOT NULL,
  `tahunsesudah` varchar(50) NOT NULL,
  `tempatkerja` varchar(100) NOT NULL,
  PRIMARY KEY (`riwayatkerja_id`),
  KEY `FK_RiwayatKerjaUsersId` (`users_id`),
  CONSTRAINT `FK_RiwayatKerjaUsersId` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riwayatkerja`
--

LOCK TABLES `riwayatkerja` WRITE;
/*!40000 ALTER TABLE `riwayatkerja` DISABLE KEYS */;
INSERT INTO `riwayatkerja` VALUES (1,1,'Direktur','Airplane Mode','01/02/2016','01/06/2020','Jiraya'),(2,1,'Software Engineer','Teknologi Abadi','01/02/2015','01/07/2020','Bekasi Raya'),(4,1,'Software Engineer','Teknologi Abadi','01/02/2015','01/07/2020','Bekasi Raya');
/*!40000 ALTER TABLE `riwayatkerja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riwayatpendidikan`
--

DROP TABLE IF EXISTS `riwayatpendidikan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `riwayatpendidikan` (
  `riwayatpendidikan_id` int NOT NULL AUTO_INCREMENT,
  `users_id` int DEFAULT NULL,
  `gelar` varchar(50) NOT NULL,
  `tempatpendidikan` varchar(100) NOT NULL,
  `tahunsebelum` varchar(50) NOT NULL,
  `tahunsesudah` varchar(50) NOT NULL,
  PRIMARY KEY (`riwayatpendidikan_id`),
  KEY `FK_RiwayatPendidikanUsersId` (`users_id`),
  CONSTRAINT `FK_RiwayatPendidikanUsersId` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riwayatpendidikan`
--

LOCK TABLES `riwayatpendidikan` WRITE;
/*!40000 ALTER TABLE `riwayatpendidikan` DISABLE KEYS */;
INSERT INTO `riwayatpendidikan` VALUES (1,1,'S! Otomotifxzx','Universitas UIIzxz','01/02/2015zxz','01/06/2020zxz'),(7,1,'S! Otomotif','Universitas UI','01/02/2015','01/06/2020');
/*!40000 ALTER TABLE `riwayatpendidikan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tokens` (
  `id` int NOT NULL AUTO_INCREMENT,
  `users_id` int NOT NULL,
  `token` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_TokenUserId` (`users_id`),
  CONSTRAINT `FK_TokenUserId` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (1,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjE0OTMwNzQ0fQ.f11xXBcDl7UIIzcDF9odg55gQC8U14objkJJJ6XCtOE'),(2,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjE0OTMwODY2fQ.URY8u2duiDOho_z8WIXOd72uHB_M2s0fKpkuVBGO3iE'),(3,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTA1M30.1cjQ6jv2ybw4If2zOuUSPJnK-9cywgJvWdDfxc9YA-0'),(4,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTE2NH0.gPyZw0fGZHsvrm-7Tk3GgrRq9bq6XCy8TJcPlQuZo4o'),(5,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTE4M30.Pmi7_H7FN-pmivOtR21SJ0RzWJL2gkRnF1OSPk7wNiI'),(6,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTIwNn0.Y_vKr02tO67cbSQguXCm1CyQrdZas6S4cNIa7vlUmok'),(7,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTI0OH0.w-fRPtL5qS5XHSqfaXivnRgnirBrZjHoEV7MySEICcM'),(8,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTI5M30.dex4MzB5it41CkiS2zbscwLKcGAGFNZlDN6dcWwkmDg'),(9,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTMxNX0.sDd5zDkMqKNY51Vx-tiMh_kkdOMxP5sF-2bnWkuFl-Q'),(10,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTMxOX0.hplfvwW9Iwilk1n02tokqnxiUGWjVQWUYUV_Po6aRdk'),(11,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTM0MH0.ItIsT1KznU95hggE6gpx4h65IE5ITkPZ858PfpawIvw'),(12,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTUzMX0.AAKZTFlVmti0GaU49K5faXeuzmkhdWldMlM6kYiGuTk'),(13,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTU1OH0.aau5LDuS-rwwOtEcHn230gq35GMnF0cVH5GyNxBDVCw'),(14,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTU4MX0.PrrxVx0zRFJLNBn4q1It1RTx49FBQgFXy36cCPphGBY'),(15,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTU5N30.1RsVk3J6JL6Wg9DQsYP8xOh5nXaKXOuRYXHhf3u8FHo'),(16,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMTcxNX0.j6olQaEbv5OlwHa9zIjgQ4-4aYIFuuKxLVr_ytaUPes'),(17,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMzIyOH0.D8n-hm1oMlqw9Gy_KqeKCdhqUqb1BDd7QyzxMZzcLMY'),(18,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzMzI3MX0.9OWpHIQrSQu6a59n0Wgpg7ssmW1XDtggH2Hce9AjdRg'),(19,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzNDk4MH0.I69ocT-G7ra7mNXK2KFmU_JtbIlGKJpH_nW0KCI92ts'),(20,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzNTA2N30.tERvQqzIMn0bqgJaokx-himTDDbhicGCmwHtE5pJDEM'),(21,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzNTMzNH0.Lzl2sGNWPDxldndArsF64RwAVy9q8nGXmcFLAJneJ6E'),(22,11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTEsImlhdCI6MTYxNDkzNTQyMX0.EMUVfMi7T69xECp_gcGXOo3d_z6EraCqTxDPb2ummfE');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `country` varchar(50) DEFAULT NULL,
  `otp` varchar(20) DEFAULT '0',
  `profile_image` varchar(100) NOT NULL DEFAULT 'default-avatar.png',
  `cover_image` varchar(100) NOT NULL DEFAULT 'default-avatar.png',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'kuntomustiko','kuntomustiko@gmail.com','0','1234',NULL,'9654','profile-default.png','cover-default.png'),(11,NULL,NULL,'081281147337','$2b$08$UBDRMPj5.PLP4aiRk.YaP.dphs.RT4bJcXp0mM3xlcq4pvNQmh4P6','indo','0','default-avatar.png','default-avatar.png');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-05 16:39:48
