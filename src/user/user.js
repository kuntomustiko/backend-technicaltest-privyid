const conn = require('../../config/database');
const router = require('express').Router();
const path = require('path');
const multer = require('multer');
const sharp = require('sharp');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// gambar
const coverImageFolder = path.join(__dirname, '../assets/coverImage');
const profileImageFolder = path.join(__dirname, '../assets/profileImage');
const galleryImageFolder = path.join(__dirname, '../assets/gallery');

const upload = multer({
  limits: {
    fileSize: 10000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error('Please upload image file (jpg, jpeg, or png'));
    }
    cb(undefined, true);
  },
});

////////////////////////////////////////////////
// USERS //
///////////////////////////////////////////////

///////////
// GET //
/////////
// READ COVER IMAGE
router.get('/user/cover/:filename', (req, res) => {
  let options = {
    root: coverImageFolder,
  };
  let filename = req.params.filename;
  res.status(200).sendFile(filename, options, function (err) {
    if (err) {
      return res.status(404).send({ message: 'Gambar tidak ditemukan' });
    }
  });
});

// READ PROFILE IMAGE
router.get('/user/profile/:filename', (req, res) => {
  let options = {
    root: profileImageFolder,
  };
  let filename = req.params.filename;
  res.status(200).sendFile(filename, options, function (err) {
    if (err) {
      return res.status(404).send({ message: 'Gambar tidak ditemukan' });
    }
  });
});

router.get('/users/:id', (req, res) => {
  const sql = `SELECT * FROM users where id = ${req.params.id}`;

  conn.query(sql, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(200).send(result);
  });
});

///////////
// POST //
/////////

// REGISTER USER
router.post('/register', (req, res) => {
  const sql = `INSERT INTO users SET ?`;
  const data = req.body;
  // phone password country

  data.password = bcrypt.hashSync(data.password, 8);

  conn.query(sql, data, (err, result) => {
    if (err) return res.send(err);
    res.send({
      message: 'Register Berhasil',
    });
  });
});

// phone password
// LOGIN USER
router.post('/user/login', (req, res) => {
  const { phone_number, password } = req.body;
  const sql = `SELECT * FROM users WHERE phone_number = '${phone_number}'`;
  const sqlToken = `INSERT INTO tokens SET ?`;

  conn.query(sql, (err, result) => {
    if (err) {
      return res.status(500).send({ error: err.sqlMessage });
    }
    let user = result[0];

    if (!user && !phone_number) {
      return res.send('phone tidak ditemukan!');
    }

    let pass = bcrypt.compareSync(password, user.password);
    if (!pass) {
      return res.send('Password Salah!');
    }
    let token = jwt.sign({ id: user.id }, 'secret_key');

    const data = { users_id: user.id, token: token };

    // Query Token
    conn.query(sqlToken, data, (err, result) => {
      if (err) {
        return res.send(err.sqlMessage);
      }
      res.send({
        message: 'Login Berhasil',
        token: token,
        user,
      });
    });
  });
});

// update kode otp yang di kirim lewat respon
router.patch('/user/kirimotp/:id', (req, res) => {
  // buat kode otp
  let kodeotpArr = [];
  for (let index = 0; index < 4; index++) {
    let kodeotp = Math.floor(Math.random() * 10);
    kodeotpArr.push(kodeotp);
  }
  let kodeotpfix = kodeotpArr.join('');
  console.log(kodeotpfix);

  const sql = `SELECT * FROM users WHERE id = ${req.params.id}`;
  const sqlUpdate = `UPDATE users SET otp = ${kodeotpfix} WHERE id = ${req.params.id}`;

  conn.query(sql, (err, result) => {
    if (err) return res.status(500).send(err);
    // otpSendEmail(result[0].name, result[0].email, kodeotpfix);
    console.log(result[0].email);

    conn.query(sqlUpdate, (err, result) => {
      if (err) return res.status(500).send(err);
    });
    res.status(200).send(kodeotpfix);
  });
});

// Mencocokan OTP
router.get('/user/cocokotp/:otp', (req, res) => {
  //   const sql = `SELECT * FROM users WHERE otp = ? AND id = '${req.params.id}'`;
  const sql = `SELECT * FROM users WHERE otp = ${req.params.otp}`;

  conn.query(sql, (err, result) => {
    if (err) return res.status(500).send(err);

    res.status(200).send(result[0]);
  });
});

// UPLOAD GAMBAR PROFILE_IMAGE DENGAN MENGUPDATE ISI TABEL
router.post(
  '/user/profile/:id',

  upload.single('profile_image'),
  async (req, res) => {
    try {
      const profilePict = `${req.params.id}-profile.png`;
      const sql = `UPDATE users SET profile_image = ? WHERE id = ?`;
      const data = [profilePict, req.params.id];
      await sharp(req.file.buffer)
        .resize(200)
        .png()
        .toFile(`${profileImageFolder}/${profilePict}`);

      conn.query(sql, data, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        }
        res.status(200).send({ message: 'berhasil', result });
      });
    } catch (err) {
      res.status(404).send(err);
    }
  },
  (err, req, res, next) => {
    res.status(500).send(err);
  }
);

// UPLOAD GAMBAR COVER_IMAGE DENGAN MENGUPDATE ISI TABEL
router.post(
  '/user/cover/:id',

  upload.single('cover_image'),
  async (req, res) => {
    try {
      const coverPict = `${req.params.id}-cover.png`;
      const sql = `UPDATE users SET cover_image = ? WHERE id = ?`;
      const data = [coverPict, req.params.id];
      await sharp(req.file.buffer)
        .resize(200)
        .png()
        .toFile(`${coverImageFolder}/${coverPict}`);

      conn.query(sql, data, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        }
        res.status(200).send({ message: 'berhasil', result });
      });
    } catch (err) {
      res.status(404).send(err);
    }
  },
  (err, req, res, next) => {
    res.status(500).send(err);
  }
);

// HAPUS DATA GAMBAR PROFILE DENGAN MENGUPDATE ROW NYA DAN DI ISI COLUMNYA DENGAN GAMBAR DEFAULT
router.patch('/user/profiledefault/:id', (req, res) => {
  const sql = `UPDATE users SET ? WHERE id = ?`;
  const data = [req.body, req.params.id];

  conn.query(sql, data, (err, result) => {
    if (err) {
      return res.status(500).send(err.sqlMessage);
    }

    res
      .status(200)
      .send({ message: 'Profile Image Kembali Ke Default', result });
  });
});

// HAPUS DATA GAMBAR COVER DENGAN MENGUPDATE ROW NYA DAN DI ISI COLUMNYA DENGAN GAMBAR DEFAULT
router.patch('/user/coverdefault/:id', (req, res) => {
  const sql = `UPDATE users SET ? WHERE id = ?`;
  const data = [req.body, req.params.id];

  conn.query(sql, data, (err, result) => {
    if (err) {
      return res.status(500).send(err.sqlMessage);
    }

    res
      .status(200)
      .send({ message: 'Profile Image Kembali Ke Default', result });
  });
});
///////////
// DELETE //
/////////

///////////
// PATCH //
/////////

router.patch('/users/editusers/:id', (req, res) => {
  const sqlUpdate = `UPDATE users SET ? WHERE id = ${req.params.id}`;
  const data = req.body;

  conn.query(sqlUpdate, data, (err, result) => {
    if (err) return res.status(500).send(err);

    res.status(200).send('data berhasil dirubah');
  });
});

////////////////////////////////////////////////
// RIWAYAT KERJA //
///////////////////////////////////////////////

//////////
// GET //
////////

// READ ALL RIWAYAT KERJA
router.get('/riwayatkerja/all/:usersid', (req, res) => {
  const sql = `SELECT * FROM riwayatkerja WHERE users_id = '${req.params.usersid}'`;
  conn.query(sql, (err, result) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(result);
  });
});

// READ DATA FOR EDITED
router.get('/riwayatkerja/readedited/:riwayatkerja_id', (req, res) => {
  const sql = `SELECT * FROM riwayatkerja WHERE riwayatkerja_id = '${req.params.riwayatkerja_id}'`;
  conn.query(sql, (err, result) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(result);
  });
});

///////////
// POST //
/////////
// INSERT DATA RIWAYAT KERJA
router.post('/riwayatkerja/adddata', (req, res) => {
  const sqlInsert = `INSERT INTO riwayatkerja SET ?`;
  const dataInsert = req.body;

  conn.query(sqlInsert, dataInsert, (err, result) => {
    if (err) return res.status(500).send(err);

    res.status(201).send(result);
  });
});

/////////////
// PATCH //
///////////
// UPDATE DATA RIWAYAT KERJA
// postman:berhasil
router.patch('/riwayatkerja/update/:riwayatkerjaid', (req, res) => {
  const sql = `UPDATE riwayatkerja SET ? WHERE riwayatkerja_id = ?`;
  const data = [req.body, req.params.riwayatkerjaid];

  conn.query(sql, data, (err, result) => {
    if (err) {
      return res.status(500).send(err.sqlMessage);
    }
    res.status(200).send({ message: 'Riwayat Kerja Berhasil di edit', result });
  });
});

/////////////
// DELETE //
///////////
// DELETE DATA RIWAYAT KERJA
router.delete('/riwayatkerja/delete/:riwayatkerjaid', (req, res) => {
  const sqlDelete = `DELETE FROM riwayatkerja WHERE riwayatkerja_id = ${req.params.riwayatkerjaid}`;
  conn.query(sqlDelete, (err, result) => {
    if (err) return res.status(500).send(err);
    res.status(200).send('data berhasil dihapus');
  });
});

////////////////////////////////////////////////
// RIWAYAT PENDIDIKAN //
///////////////////////////////////////////////
//////////
// GET //
////////

// READ RIWAYAT PENDIDIKAN ALL
router.get('/riwayatpendidikan/all/:usersid', (req, res) => {
  const sql = `SELECT * FROM riwayatpendidikan WHERE users_id = '${req.params.usersid}'`;

  conn.query(sql, (err, result) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(result);
  });
});

// READ DATA FOR EDITED
router.get(
  '/riwayatpendidikan/readedited/:riwayatpendidikan_id',

  (req, res) => {
    const sql = `SELECT * FROM riwayatpendidikan WHERE riwayatpendidikan_id = '${req.params.riwayatpendidikan_id}'`;
    conn.query(sql, (err, result) => {
      if (err) return res.status(500).send(err);
      res.status(200).send(result);
    });
  }
);

///////////
// POST //
/////////

// INSERT DATA RIWAYAT PENDIDIKAN
router.post('/riwayatpendidikan/adddata', (req, res) => {
  const sqlInsert = `INSERT INTO riwayatpendidikan SET ?`;
  const dataInsert = req.body;

  conn.query(sqlInsert, dataInsert, (err, result) => {
    if (err) return res.status(500).send(err);

    res.status(201).send(result);
  });
});

///////////
// PATCH //
/////////

// UPDATE DATA RIWAYAT PENDIDIKAN
// postman:berhasil
router.patch(
  '/riwayatpendidikan/update/:riwayatpendidikanid',

  (req, res) => {
    const sql = `UPDATE riwayatpendidikan SET ? WHERE riwayatpendidikan_id = ?`;
    const data = [req.body, req.params.riwayatpendidikanid];

    conn.query(sql, data, (err, result) => {
      if (err) {
        return res.status(500).send(err.sqlMessage);
      }
      res
        .status(200)
        .send({ message: 'Riwayat Pendidikan Berhasil di edit', result });
    });
  }
);

/////////////
// DELETE //
///////////

// DELETE DATA RIWAYAT PENDIDIKAN
router.delete(
  '/riwayatpendidikan/delete/:riwayatpendidikanid',

  (req, res) => {
    const sqlDelete = `DELETE FROM riwayatpendidikan WHERE riwayatpendidikan_id = ${req.params.riwayatpendidikanid}`;
    conn.query(sqlDelete, (err, result) => {
      if (err) return res.status(500).send(err);
      res.status(200).send('data berhasil dihapus');
    });
  }
);

////////////////////////////////////////////////
// GALLERY //
///////////////////////////////////////////////
//////////
// GET //
////////

// READ DATA GALLERY
router.get('/gallery/all/:usersid', (req, res) => {
  const sql = `SELECT * FROM gallery WHERE users_id = '${req.params.usersid}'`;

  conn.query(sql, (err, result) => {
    if (err) return res.status(500).send(err);
    res.status(200).send(result);
  });
});

// READ ONLY GALLERY IMAGE - sudah bisa
router.get('/gallery/:filename', (req, res) => {
  let options = {
    root: galleryImageFolder,
  };
  let filename = req.params.filename;
  res.status(200).sendFile(filename, options, function (err) {
    if (err) {
      return res.status(404).send({ message: 'Gambar tidak ditemukan' });
    }
  });
});

/////////////
// DELETE //
///////////
router.delete('/gallery/delete/:gallery_id', (req, res) => {
  const sql = `DELETE FROM gallery WHERE gallery_id = ${req.params.gallery_id}`;

  conn.query(sql, (err, result) => {
    if (err) {
      return res.status(500).send(err.sqlMessage);
    }

    res.status(200).send({ message: 'Product Berhasil di hapus', result });
  });
});

///////////
// POST //
/////////

router.post(
  '/user/gallery/:id',

  upload.array('gallery'),
  async (req, res) => {
    try {
      const coverPict = `abcde-cover.png`;

      req.body.images = [];

      await Promise.all(
        req.file.map(async (file) => {
          const filename = file.originalname.replace(/\..+$/, '');
          const newFilename = `cover-${filename}-${Date.now()}.jpeg`;
          await sharp(file.buffer)
            .resize(200)
            .png()
            .toFile(`${coverImageFolder}/${newFilename}`);
          req.body.images.push([req.params.id, newFilename]);
        })
      );

      //   const sql = `UPDATE gallery SET cover_image = ? WHERE id = ?`;
      const sql = `INSERT INTO gallery(users_id, gallery_image) VALUES ?`;
      //   const data = [[1, coverPict]];
      //   const data = [coverPict, req.params.id];
      console.log(req.body.images);

      conn.query(sql, [req.body.images], (err, result) => {
        if (err) {
          return res.status(500).send(err);
        }
        res.status(200).send({ message: 'berhasil', result });
      });
    } catch (err) {
      res.status(404).send(err);
    }
  },
  (err, req, res, next) => {
    res.status(500).send(err);
  }
);

module.exports = router;
