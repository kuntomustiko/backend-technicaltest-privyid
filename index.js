const express = require('express');
const cors = require('cors');
const app = express();
const port = 2020;

// const swaggerJsDoc = require('swagger-jsdoc');
// const swaggerUi = require('swagger-ui-express');

// USER ROUTES
const user = require('./src/user/user.js');

app.use(cors());
app.use(express.json());
app.use(user);

// const swaggerOptions = {
//   swaggerDefinition: {
//     info: {
//       title: 'Technical Test',
//       description: 'Technical Test PrivyId Kunto Mustiko',
//       contact: {
//         name: 'Kunto Mustiko',
//       },
//       servers: ['http://localhost:2020'],
//     },
//   },
//   apis: ['./src/user/user.js'],
// };

// const swaggerDocs = swaggerJsDoc(swaggerOptions);
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// app.get("/customers",(req,res) => {
//   res.status(200).send("customer results")
// })

// Routes
/**
 * @swagger
 * /customers:
 * get:
 *    description: Use to request all customers
 *    responses:
 *      '200':
 *        description: A successfull response
 *
 */

app.get('/', (req, res) => {
  res.send(`<h1>API Running at Port : ${port}</h1>`);
});

app.listen(port, () => {
  console.log(`API Running at ${port}`);
});
