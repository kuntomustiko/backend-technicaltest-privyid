const jwt = require('jsonwebtoken');
const conn = require('../../database');

// JWT AUTH
// Auth Function
const auth = (req, res, next) => {
  // Mengambil token saat proses menerima request
  let token = req.header('Authorization');

  // Mencoba mengambil data yang tersimpan di token
  let decoded = jwt.verify(token, 'secret_key');

  let sql = `SELECT * FROM users WHERE id = ${decoded.id}`;

  conn.query(sql, (err, result) => {
    if (err) return res.send(err);

    req.user = result[0];
    next();
  });
};

module.exports = auth;
